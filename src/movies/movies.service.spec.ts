import { Test } from '@nestjs/testing';
import { MoviesService } from './movies.service';
import { Movie } from './entities/movie.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { DataSource, Repository } from 'typeorm';
import { InMemoryDatabase } from '../../test/utils/memory-database';
import { MovieFactory } from '../../test/factories/movies.factory';

describe('MoviesService', () => {
  let service: MoviesService;
  let connection: DataSource;
  let repository: Repository<Movie>;
  let inMemoryDb: InMemoryDatabase;

  beforeEach(async () => {
    await Test.createTestingModule({
      providers: [
        MoviesService,
        {
          provide: getRepositoryToken(Movie),
          useClass: Repository,
        },
      ],
    }).compile();

    inMemoryDb = new InMemoryDatabase();

    connection = inMemoryDb.connection;

    await connection.initialize();
    await connection.synchronize();

    repository = connection.getRepository(Movie);
    service = new MoviesService(repository);
  });

  afterEach(async () => {
    await connection.destroy();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('findAll', () => {
    it('should return a list of movies', async () => {
      // Given I have 2 movies in the DB
      await repository.save(MovieFactory.createNewMovie());

      await repository.save(MovieFactory.createNewMovie());

      // When I try to get all of them
      const movies: Movie[] = await service.findAll();

      // Then I should find a list of 2 elements
      expect(movies.length).toBe(2);
    });
  });
});
