import { Media } from '../../media/entity/media.entity';
import { Column, Entity } from 'typeorm';

@Entity({ name: 'movies' })
export class Movie extends Media {
  @Column({ type: 'integer' })
  duration: number;

  @Column({ type: 'timestamp with time zone', name: 'release_date' })
  releaseDate: Date;
}
