import {
  IsArray,
  IsDateString,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';

export class CreateMovieDto {
  @IsString()
  @IsOptional()
  cover?: string;

  @IsNumber()
  @IsNotEmpty()
  duration: number;

  @IsArray()
  @IsString({ each: true })
  @IsNotEmpty()
  genres: string[];

  @IsDateString()
  @IsNotEmpty()
  releaseDate: Date;

  @IsString()
  @IsOptional()
  synopsis?: string;

  @IsString()
  @IsNotEmpty()
  title: string;
}
