import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateMovieDto } from './dto/create-movie.dto';
import { UpdateMovieDto } from './dto/update-movie.dto';
import { Movie } from './entities/movie.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class MoviesService {
  constructor(
    @InjectRepository(Movie)
    private moviesRepository: Repository<Movie>,
  ) {}

  /**
   * Create a new movie
   * @param {CreateMovieDto} createMovieDto Movie data
   * @returns {Promise<number>} Movie id
   */
  async create(createMovieDto: CreateMovieDto): Promise<Movie> {
    const movie = this.moviesRepository.create(createMovieDto);

    return await this.moviesRepository.save(movie);
  }

  /**
   * Return all movies
   * @returns {Promise<Movie[]>} All movies
   */
  async findAll(): Promise<Movie[]> {
    return this.moviesRepository.find();
  }

  /**
   * Return a single movie
   * @param {number} id Movie id
   * @returns {Promise<Movie>} A single movie
   * @throws {NotFoundException} Movie not found
   */
  async findOne(id: number): Promise<Movie> {
    const movie = await this.moviesRepository.findOne({
      where: {
        id,
      },
    });

    if (!movie) throw new NotFoundException(`Movie #${id} not found`);

    return movie;
  }

  /**
   * Update a movie
   * @param {number} id Movie id
   * @param {UpdateMovieDto} updateMovieDto Movie data
   * @returns {Promise<void>}
   */
  async update(id: number, updateMovieDto: UpdateMovieDto): Promise<void> {
    const movie = await this.moviesRepository.findOne({
      where: {
        id,
      },
    });

    if (!movie) throw new NotFoundException(`Movie #${id} not found`);

    await this.moviesRepository.save({ ...updateMovieDto, id });
  }

  /**
   * Remove a movie
   * @param {number} id Movie id
   * @returns {Promise<void>}
   */
  async remove(id: number): Promise<void> {
    const movie = await this.moviesRepository.findOne({
      where: {
        id,
      },
    });

    if (!movie) throw new NotFoundException(`Movie #${id} not found`);

    await this.moviesRepository.delete({
      id,
    });
  }
}
