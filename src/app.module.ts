import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MoviesModule } from './movies/movies.module';

import postgresql from './config/postgresql';
import { Media } from './media/entity/media.entity';
import { Movie } from './movies/entities/movie.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      ...postgresql,
      type: 'postgres',
      entities: [Media, Movie],
    }),
    MoviesModule,
  ],
})
export class AppModule {}
