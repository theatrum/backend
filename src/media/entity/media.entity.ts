import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export abstract class Media {
  @PrimaryGeneratedColumn({ type: 'int' })
  id?: number;

  @Column({ type: 'varchar', length: 100 })
  title: string;

  @Column({ type: 'varchar', length: 255 })
  cover: string;

  @Column({ type: 'text', array: true })
  genres: Array<string>;

  @Column({ type: 'text', nullable: true })
  synopsis?: string;
}
