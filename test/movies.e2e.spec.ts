import { Test, TestingModule } from '@nestjs/testing';
import { MoviesController } from '../src/movies/movies.controller';
import { Movie } from '../src/movies/entities/movie.entity';
import { DataSource, Repository } from 'typeorm';
import { InMemoryDatabase } from './utils/memory-database';
import * as request from 'supertest';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import { AppModule } from '../src/app.module';
import { CreateMovieDto } from 'src/movies/dto/create-movie.dto';
import { MovieFactory } from './factories/movies.factory';

describe('MoviesController (e2e)', () => {
  let controller: MoviesController;
  let app: INestApplication;
  let repository: Repository<Movie>;
  let inMemoryDb: InMemoryDatabase;
  let connection: DataSource;

  let movie1: Movie;
  let movie2: Movie;

  beforeEach(async () => {
    inMemoryDb = new InMemoryDatabase();

    connection = inMemoryDb.connection;

    await connection.initialize();
    await connection.synchronize();

    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    })
      .overrideProvider(DataSource)
      .useValue(connection)
      .compile();

    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(
      new ValidationPipe({
        whitelist: true,
        transform: true,
      }),
    );
    await app.init();

    repository = connection.getRepository(Movie);
    controller = moduleFixture.get<MoviesController>(MoviesController);

    movie1 = MovieFactory.createNewMovie();
    movie2 = MovieFactory.createNewMovie();
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('(GET) /movies', () => {
    it('should return a 200 and a list of movies if there is some', async () => {
      // Given I have 2 movies in the DB
      await repository.save([movie1, movie2]);

      // When I call the endpoint to get all movies
      const result = await request(app.getHttpServer()).get('/movies');

      // Then I should find have a 200
      expect(result.status).toBe(200);

      // And a list of 2 movie
      expect(result.body.length).toBe(2);
    });

    it('should return a 200 and an empty list of movies if there is none', async () => {
      // Given I have no movie in the DB

      // When I call the endpoint to get all movies
      const result = await request(app.getHttpServer()).get('/movies');

      // Then I should find have a 200
      expect(result.status).toBe(200);

      // And a list of 2 movie
      expect(result.body.length).toBe(0);
    });
  });

  describe('(GET) /movies/:id', () => {
    it('should return a 200 and a movie if movie found', async () => {
      // Given I have a movie with an ID in the db
      const movie: Movie = await repository.save(movie1);

      // When I call the endpoint to get a movie using its id
      const result = await request(app.getHttpServer()).get(
        `/movies/${movie.id}`,
      );

      // Then I should find have a 200
      expect(result.status).toBe(200);

      // And I should get the movie
      expect(result.body).toStrictEqual({
        ...movie,
        releaseDate: movie.releaseDate.toISOString(),
      });
    });

    it('should return a 404 if movie not found', async () => {
      // Given I have no movie in the db

      // When I call the endpoint to get a movie using its id
      const result = await request(app.getHttpServer()).get('/movies/1');

      // Then I should find have a 404
      expect(result.status).toBe(404);
    });
  });

  describe('(POST) /movies', () => {
    it('should return a 201 when the movie is created', async () => {
      // Given I have a valid movie entity
      const movie: CreateMovieDto = {
        cover: '',
        genres: [],
        title: 'Matrix',
        duration: 150,
        releaseDate: new Date(),
        synopsis: null,
      };

      // When I call the endpoint to create a movie
      const result = await request(app.getHttpServer())
        .post('/movies')
        .send(movie);

      // Then it should return a 201
      expect(result.statusCode).toBe(201);

      // And I should have the movie as an answer with its code
      expect(result.body).toEqual({
        ...movie,
        releaseDate: movie.releaseDate.toISOString(),
        id: result.body.id,
      });

      // And I should find it in the repository
      const movieFound: Movie = await repository.findOne({
        where: {
          id: result.body.id,
        },
      });

      expect(movieFound).toEqual({
        ...movie,
        id: result.body.id,
      });
    });

    it('should return a 400 when the movie is movie is missing field(s)', async () => {
      // Given I have a invalid movie entity
      const incompleteMovie = {
        cover: '',
        genres: [],
        title: 'Matrix',
      };

      // When I call the endpoint to create a movie
      const result = await request(app.getHttpServer())
        .post('/movies')
        .send(incompleteMovie);

      // Then it should return a 400
      expect(result.statusCode).toBe(400);
    });
  });

  describe('(DELETE) /movies/:id', () => {
    it('should return a 200 if movie found', async () => {
      // Given I have a movie with an ID in the db
      const movie: Movie = await repository.save(movie1);

      // When I call the endpoint to delete a movie using its id
      const result = await request(app.getHttpServer()).delete(
        `/movies/${movie.id}`,
      );

      // Then I should find have a 200
      expect(result.status).toBe(200);

      // And I should no longer find the movie in the DB
      const movieFound: Movie = await repository.findOne({
        where: {
          id: movie.id,
        },
      });
      expect(movieFound).toBeFalsy();
    });

    it('should return a 404 if movie not found', async () => {
      // Given I have no movie in the db

      // When I call the endpoint to delete a movie using its id
      const result = await request(app.getHttpServer()).delete('/movies/1');

      // Then I should find have a 404
      expect(result.status).toBe(404);
    });
  });

  describe('(PUT) /movies/:id', () => {
    it('should return a 200 if movie found', async () => {
      // Given I have a movie with an ID in the db
      const movie: Movie = await repository.save(movie1);

      // When I call the endpoint to update a movie using its id
      const result = await request(app.getHttpServer())
        .put(`/movies/${movie.id}`)
        .send({
          duration: 180,
        });

      // Then I should find have a 200
      expect(result.status).toBe(200);

      // And I should have the new values in the DB
      const movieFound: Movie = await repository.findOne({
        where: {
          id: movie.id,
        },
      });
      expect(movieFound.duration).toStrictEqual(180);
    });

    it('should return a 404 if movie not found', async () => {
      // Given I have no movie in the db

      // When I call the endpoint to get all movies
      const result = await request(app.getHttpServer()).put('/movies/1');

      // Then I should find have a 404
      expect(result.status).toBe(404);
    });
  });
});
