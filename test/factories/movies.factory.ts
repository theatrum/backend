import { Movie } from '../../src/movies/entities/movie.entity';
import { faker } from '@faker-js/faker';

export class MovieFactory {
  static createNewMovie(): Movie {
    const cover = faker.image.urlPicsumPhotos();
    const title = faker.lorem.words(2);
    const duration = faker.number.int({ min: 60, max: 180 });
    const genres = faker.helpers.arrayElements(
      ['Action', 'Comedy', 'Drama', 'Horror'],
      2,
    );
    const releaseDate = faker.date.recent();

    return {
      cover,
      title,
      duration,
      genres,
      releaseDate,
    };
  }
}
