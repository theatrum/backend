import { DataType, IMemoryDb, newDb } from 'pg-mem';
import { Media } from '../../src/media/entity/media.entity';
import { Movie } from '../../src/movies/entities/movie.entity';
import { DataSource } from 'typeorm';
import { v4 } from 'uuid';

export class InMemoryDatabase {
  db: IMemoryDb;
  connection: DataSource;

  constructor() {
    this.db = newDb({
      autoCreateForeignKeyIndices: true,
    });

    this.db.public.registerFunction({
      name: 'current_database',
      args: [],
      returns: DataType.text,
      implementation: (x) => `hello world: ${x}`,
    });

    this.db.public.registerFunction({
      name: 'version',
      args: [],
      returns: DataType.text,
      implementation: (x) => `hello world: ${x}`,
    });

    this.db.registerExtension('uuid-ossp', (schema) => {
      schema.registerFunction({
        name: 'uuid_generate_v4',
        returns: DataType.uuid,
        implementation: v4,
        impure: true,
      });
    });

    this.db.public.registerFunction({
      name: 'obj_description',
      args: [DataType.text, DataType.text],
      returns: DataType.text,
      implementation: () => 'test',
    });

    this.connection = this.db.adapters.createTypeormDataSource({
      type: 'postgres',
      entities: [Media, Movie],
    });
  }
}
