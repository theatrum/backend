################################################################################
FROM node:20-alpine As base

ARG NODE_ENV
ARG GID=1000
ARG UID=1000
ARG USER=theatrum

RUN apk add shadow && \
    deluser node && \
    groupadd -f -g ${GID} ${USER} && \
    useradd -m -g ${USER} -u ${UID} ${USER}

WORKDIR /app
RUN chown -R ${USER}:${USER} .
RUN npm install -g @nestjs/cli

USER ${USER}
COPY --chown=${USER}:${USER} . .

RUN if [ "${NODE_ENV}" = "production" ]; then \
  npm clean-install --omit=dev && npm cache clean --force; \
  npm run build;\
  fi

###############################################################################
FROM base as development

ENV NODE_ENV=development
RUN npm install

EXPOSE 3000

CMD [ "npm", "run", "start:dev"]

###############################################################################
FROM node:20-alpine as production

WORKDIR /app

COPY --from=base /app/package.json .
COPY --from=base /app/tsconfig.json .
COPY --from=base /app/dist ./dist
COPY --from=base /app/node_modules ./node_modules

EXPOSE 3000

CMD [ "node", "dist/main.js" ]